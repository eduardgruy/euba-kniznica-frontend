import React, { useState } from "react";
import { backend_server, backend_port } from "../settings";

import {
  Stack,
  TextField,
  PrimaryButton,
  IStackStyles,
  Text,
  IButtonStyles,
} from "office-ui-fabric-react";

type Props = {
  setUser: (user: string) => void;
};

const containerStyle: IStackStyles = {
  root: {
    width: "100vw",
    height: "100vh",
    backgroundColor: "#F3F2F1",
  },
};
const textStyle = {
  root: {
    width: 250,
    marginBottom: 10,
  },
};

const buttonStyle: IButtonStyles = {
  root: {
    width: 250,
    marginTop: "30px !important",
  },
};

const loginBoxStyle: IStackStyles = {
  root: {
    width: "350px",
    padding: "40px",
    backgroundColor: "white",
  },
};

const Login = (props: Props) => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const handleLogin = async () => {
    if (username.trim().length === 0) return;
    if (password.trim().length === 0) return;

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ username: username, password: password }),
    };

    await fetch(`https://${backend_server}:${backend_port}/users/authenticate`, requestOptions)
      .then(handleResponse)
      .then((username) => {
        if (username) {
          localStorage.setItem("user", username);
          props.setUser(username);
        }

        return username;
      });
    return;
  };

  const handleResponse = (response: any) => {
    return response.text().then((text: any) => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
          window.alert("Zlé meno alebo heslo!")
          return 
        }
        return data.username;
    });
}
  const handleKeyDown = (e: any) => {
    e.key === "Enter" && handleLogin();
  };
  return (
    <>
      <Stack
        horizontalAlign="center"
        verticalAlign="center"
        styles={containerStyle}
      >
        <Stack horizontalAlign="center" styles={loginBoxStyle}>
          <Text variant="large">Knižnica</Text>
          <form onKeyPress={handleKeyDown}>
            <TextField
              onChange={(_, newValue) => setUsername(newValue || "")}
              value={username}
              styles={textStyle}
              placeholder="Meno"
              iconProps={{ iconName: "Contact" }}
            />
            <TextField
              onChange={(_, newValue) => setPassword(newValue || "")}
              value={password}
              type='password'
              styles={textStyle}
              placeholder="Heslo"
              iconProps={{ iconName: "Contact" }}
            />

            <PrimaryButton
              data-automation-id="test"
              text="Prihlásiť"
              onClick={handleLogin}
              styles={buttonStyle}
            />
          </form>
        </Stack>
      </Stack>
    </>
  );
};

export default Login;
