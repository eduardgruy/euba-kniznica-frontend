const Speech = (getMenu) => {
  window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition || window.mozSpeechRecognition;
  const recognition = new window.SpeechRecognition();

  recognition.lang = "sk-SK"; // slovensky jazyk
  recognition.interimResults = false;
  recognition.maxAlternatives = 5;
  recognition.onresult = function (event) {
    // keď rozpoznas slova, posli ich do funkcie getMenu

    const result = event.results[0][0].transcript;
    getMenu(result);
    console.log(result);
  };
  recognition.start();
};

export default Speech;
