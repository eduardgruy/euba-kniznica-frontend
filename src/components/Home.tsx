import React from "react";
import { Stack } from "@fluentui/react";
import MainButton from "../lib/MainButton";

type Props = {
  user: string | undefined
}

const Home = (props: Props) => {
  return (
    <Stack horizontalAlign="center" verticalAlign="center">
      <MainButton user={props.user} />
    </Stack>
  );
};
export default Home;
