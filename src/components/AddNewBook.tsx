import React, { useState, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import {backend_server ,backend_port} from "../settings"
import {
  Stack,
  TextField,
  PrimaryButton,
  Separator,
  IStackTokens,
  IStackStyles,
  IStackItemStyles,
  MessageBar,
  MessageBarType,
} from "@fluentui/react";

function Dropzone(props: any) {
  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file: any) => {
      const reader = new FileReader();

      reader.onabort = () => console.log("file reading was aborted");
      reader.onerror = () => console.log("file reading has failed");
      reader.onload = () => {
        props.setNewPayload(reader.result);
      };
      reader.readAsArrayBuffer(file);
    });
  }, []);
  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    onDrop,
  });
  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));
  return (
    <div className="container">
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <p></p>
        <p>Súbor knihy potiahnite sem</p>
        <p></p>
        <button type="button" onClick={open}>
          Otvoriť správcu súborov
        </button>
      </div>
      <aside>
        <h4>Súbor</h4>
        <ul>{files}</ul>
      </aside>
    </div>
  );
}

type Props = {
  addToBooks: (book: string) => void;
}

const AddNewBook = (props: Props) => {
  const [newAuthor, setNewAuthor] = useState<string>("");
  const [newTitle, setNewTitle] = useState<string>("");
  const [newCategory, setNewCategory] = useState<string>("");
  const [newYear, setNewYear] = useState<string>("");
  const [newDescription, setNewDescription] = useState<string>("");
  const [newType, setNewType] = useState<string>("");
  const [newPayload, setNewPayload] = useState<any>("");
  const [successMessage, setSuccessMessage] = useState<Boolean>(false);
  const [failedMessage, setFailedMessage] = useState<Boolean>(false);

  const clearNewBook = () => {
    setNewAuthor("");
    setNewTitle("");
    setNewCategory("");
    setNewYear("");
    setNewDescription("");
    setNewType("");
    setNewPayload("");
  };
  const addBook = () => {
    const buf = new (Buffer as any).from(newPayload, "base64");
    const newBookData = {
      author: newAuthor,
      title: newTitle,
      category: newCategory,
      year: newYear,
      description: newDescription,
      type: newType,
      payload: buf,
    };
    setSuccessMessage(false);
    setFailedMessage(false);
    fetch(`https://${backend_server}:${backend_port}/books`, {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newBookData),
    })
      .then((response: any) => {
        clearNewBook();
        setSuccessMessage(true);
        return response.json()
      })
      .then(result => {
        props.addToBooks(result.book);
      })
      .catch((err) => {
        setFailedMessage(true);
        console.log(err)
      });
  };
  // Tokens definition
  const stackStyles: IStackStyles = {
    root: {
      width: "40%",
      alignSelf: "center",
      justifyContent: "center",
    },
  };
  const stackItemStyles: IStackItemStyles = {
    root: {
      alignItems: "center",
      display: "flex",
      justifyContent: "center",
      flexFlow: "column nowrap",
    },
  };

  // Tokens definition
  const stackTokens: IStackTokens = {
    childrenGap: 5,
    padding: 10,
  };
  const SuccessMessage = () => (
    <MessageBar messageBarType={MessageBarType.success} isMultiline={false}>
      Upload úspešný!
    </MessageBar>
  );
  const FailedMessage = () => (
    <MessageBar messageBarType={MessageBarType.error} isMultiline={false}>
      Upload zlyhal
    </MessageBar>
  );

  return (
    <div>
      <Separator><h1>Pridaj novú knihu</h1></Separator>
      <Stack>
        <Stack horizontal tokens={stackTokens} styles={stackStyles}>
          <Stack.Item grow styles={stackItemStyles}>
            <TextField
              label="Autor"
              id="author"
              value={newAuthor}
              onChange={(e) =>
                setNewAuthor((e.target as HTMLInputElement).value)
              }
            />

            <TextField
              label="Názov"
              id="title"
              value={newTitle}
              onChange={(e) =>
                setNewTitle((e.target as HTMLInputElement).value)
              }
            />

            <TextField
              label="Kategória"
              id="category"
              value={newCategory}
              onChange={(e) =>
                setNewCategory((e.target as HTMLInputElement).value)
              }
            />

            <TextField
              label="Rok"
              id="year"
              value={newYear}
              onChange={(e) => setNewYear((e.target as HTMLInputElement).value)}
            />

            <TextField
              label="Popis"
              id="description"
              value={newDescription}
              onChange={(e) =>
                setNewDescription((e.target as HTMLInputElement).value)
              }
            />
            <TextField
              label="Typ"
              id="type"
              value={newType}
              onChange={(e) => setNewType((e.target as HTMLInputElement).value)}
            />
          </Stack.Item>
          <Stack.Item grow styles={stackItemStyles}>
            <Dropzone
              setNewPayload={setNewPayload}
            />
            <PrimaryButton color="primary" onClick={addBook}>
              Pridať knihu
            </PrimaryButton>{" "}
            {successMessage === true && <SuccessMessage />}
            {failedMessage === true && <FailedMessage />}
          </Stack.Item>
        </Stack>
      </Stack>
    </div>
  );
};
export default AddNewBook;
