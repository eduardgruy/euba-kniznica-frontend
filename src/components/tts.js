import xmlbuilder from "xmlbuilder";
import {azure_key} from "../settings"
const Tts = (tts) => {
  const ttsEndpoint =
    "https://northeurope.api.cognitive.microsoft.com/sts/v1.0/issuetoken";
  const ttsKey = azure_key;

  const getToken = async () => {
    const response = await fetch(ttsEndpoint, {
      method: "post",
      headers: { "Ocp-Apim-Subscription-Key": ttsKey }
    }).then(res => res.text());

    return response;
  };

  const locale = "sk-SK-Filip";
  var context; // Audio context
  var buf; // Audio buffer
  function init() {
    if (!window.AudioContext) {
      if (!window.webkitAudioContext) {
        alert(
          "Your browser does not support any AudioContext and cannot play back this audio."
        );
        return;
      }
      window.AudioContext = window.AudioContext || window.webkitAudioContext;;
    }
    context = new AudioContext();
  }

  function playByteArray(byteArray) {
    context.decodeAudioData(byteArray, function(buffer) {
      buf = buffer;
      play();
    });
  }

  // Play the loaded file
  function play() {
    // Create a source node from the buffer
    var source = context.createBufferSource();
    source.buffer = buf;
    // Connect to the final output node (the speakers)
    source.connect(context.destination);
    // Play immediately
    source.start(0);
  }

  const getTTS = async () => {
    init();
    // Vytvor XML s poziadavkou
    const xml_body = xmlbuilder
      .create("speak")
      .att("version", "1.0")
      .att("xml:lang", "sk-SK")
      .ele("voice")
      .att("xml:lang", "sk-SK")
      .att("name", locale) // sk-SK-Filip
      .txt(tts)
      .end();
    const body = xml_body.toString(); 
    // odosli poziadavku na Cognitive Services
    fetch("https://northeurope.tts.speech.microsoft.com/cognitiveservices/v1", {
      method: "post",
      headers: {
        // Vyžiadaj si autentifikáciu
        Authorization: "Bearer " + (await getToken()), 
        "Content-type": "application/ssml+xml",
        "X-Microsoft-OutputFormat": "audio-24khz-48kbitrate-mono-mp3",
        "User-Agent": "test"
      },
      body: body // obsah poziadavky
    })
      .then(response => response.arrayBuffer())
  //prehraj odpoved
      .then(response => {
        console.log(response);
        playByteArray(response);
      });
  };
  // console.log(tts)
  getTTS();
};
export default Tts;
