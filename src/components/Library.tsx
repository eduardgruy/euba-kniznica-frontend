import React, { useState, useEffect } from "react";
import {
  Stack,
  DetailsList,
  DetailsListLayoutMode,
  IStackTokens,
} from "@fluentui/react";
import AddNewBook from "./AddNewBook";
import Axios from "axios";
import {backend_server, backend_port} from "../settings"

const tokens: IStackTokens = {
  childrenGap: 20,
};

const Library = () => {
  const [books, setBooks] = useState<string[]>([""]);
  
  useEffect(() => {
    Axios.get(`https://${backend_server}:${backend_port}/books`).then((response) =>
      setBooks(response.data)
    );
  }, []);
  const columns = [
    {
      key: "column1",
      name: "Autor",
      fieldName: "author",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
    },
    {
      key: "column2",
      name: "Názov",
      fieldName: "title",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
    },
    {
      key: "column3",
      name: "Kategória",
      fieldName: "category",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
    },
    {
      key: "column4",
      name: "Rok",
      fieldName: "year",
      minWidth: 100,
      maxWidth: 30,
      isResizable: true,
    },
    {
      key: "column5",
      name: "Popis",
      fieldName: "description",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
    },
    {
      key: "column6",
      name: "Typ",
      fieldName: "type",
      minWidth: 100,
      maxWidth: 30,
      isResizable: true,
    },
  ];


  return (
    <div>
      <Stack
        tokens={tokens}
        styles={{ root: { marginTop: "60px", alignItems: "center", } }}
        horizontalAlign="start"
      >
        <DetailsList
          items={books}
          compact={true}
          layoutMode={DetailsListLayoutMode.justified}
          disableSelectionZone={true}
          columns={columns}
          setKey="_id"
          styles={{root :{height: "400px",}}}
        />
      </Stack>

      <AddNewBook
        addToBooks={(book: any) => {
          setBooks(books.concat(book));
        }}
      />
    </div>
  );
};

export default Library;
