import React from "react";
import { Link } from "react-router-dom";
import {
  Stack,
  Callout,
  Text,
  Icon,
  FontWeights,
  getTheme
} from "@fluentui/react";
import userContext from "../lib/user";

// import userContext from "../contexts/userContext";
const theme = getTheme();

const navStyle = {
  root: {
    width: "100%",
    height: "45px",
    padding: "0px 23px 0px 23px",
    backgroundColor: theme.palette.themePrimary
  }
};

type Props = {
  setUser: (username: any) => void;
};

export interface ICalloutBasicExampleState {
  isCalloutVisible?: boolean;
}

const NavBar = (props: Props) => {
  const [user] = React.useContext(userContext);
  const _menuButtonElement = React.useRef<HTMLDivElement | null>(null);

  const [isCalloutVisible, setisCalloutVisible] = React.useState<boolean>(true);

  const _onShowMenuClicked = (): void => {
    setisCalloutVisible(!isCalloutVisible);
  };
  const onLogout = () => {
    props.setUser(undefined);
    localStorage.removeItem("user");
  };
  return (
    <Stack
      styles={navStyle}
      horizontal
      horizontalAlign="space-between"
      verticalAlign="center"
    >
      <Stack horizontal verticalAlign="center" gap={13}>
        <Link to="/home">
          <Text
            variant="xLarge"
            styles={{
              root: {
                fontWeight: FontWeights.semibold,
                color: theme.palette.white
              }
            }}
          >
            Domov
          </Text>
        </Link>
        <Link to="/library">
          <Text
            variant="xLarge"
            styles={{
              root: {
                fontWeight: FontWeights.semibold,
                color: theme.palette.white
              }
            }}
          >
            Knižnica
          </Text>
        </Link>
      </Stack>
      <Stack>
        <Stack.Item>
          <div
            ref={_menuButtonElement}
            style={{ color: "white", cursor: "pointer" }}
            // onClick={props.onLogout}
            onClick={_onShowMenuClicked}
          >
            {`${user.toLowerCase()}`}
          </div>
          <Callout
            className="ms-CalloutExample-callout"
            //   ariaLabelledBy={this._labelId}
            //   ariaDescribedBy={this._descriptionId}
            role="alertdialog"
            gapSpace={0}
            target={_menuButtonElement.current}
            onDismiss={_onShowMenuClicked}
            setInitialFocus={true}
            hidden={isCalloutVisible}
          >
            <Stack
              horizontal
              horizontalAlign="space-between"
              verticalAlign="center"
              onClick={onLogout}
              styles={{
                root: { padding: "15px", width: "120px", cursor: "pointer" }
              }}
            >
              <Text>logout</Text>
              <Icon iconName="SignOut"></Icon>
            </Stack>
          </Callout>
        </Stack.Item>
      </Stack>
    </Stack>
  );
};

// export default withRouter(NavBar);
export default NavBar;
