import Tts from "../components/tts";
import { Literature } from "../interfaces/literature.interface";
import {backend_server, backend_port} from "../settings"

interface bookText {
  value: string;
  offset: number;
}
interface literature {
  _id: string;
  offset: number;
}

const readBookWithOffset = async (bookID: string, offset: number) => {
  const bookText: bookText = await fetch(
    `https://${backend_server}:${backend_port}/books/${bookID}/read/${offset}`
  )
    .then((response) => {
      return response.json();
    })
    .then((data: any) => {
      return data.response;
    });
  Tts(bookText.value);
  return bookText.offset;
};

const saveNewOffset = async (
  userID: string | undefined,
  literature: Literature,
  newOffset: number
) => {
  literature.offset = newOffset;
  await fetch(`https://${backend_server}:${backend_port}/users/${userID}/literature`, {
    method: "put",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(literature),
  }).then((response: any) => {
    console.log(response.json());
  });
};

const readBook = async (userID: string | undefined, literature: Literature) => {
  // prečítaj knihu s odsadením
  const newOffset = await readBookWithOffset(literature._id, literature.offset);
  // ulož nové odsadenie
  saveNewOffset(userID, literature, newOffset);
  return newOffset;
};
export default readBook;
