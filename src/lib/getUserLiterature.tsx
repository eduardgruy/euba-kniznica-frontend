import { Literature } from "../interfaces/literature.interface"
import {backend_server, backend_port} from "../settings"


const getUserLiterature = async (user: string | undefined) => {
    const UsersLiterature: Literature[] = await fetch(
      `https://${backend_server}:${backend_port}/users/${user}/literature`
    ).then(response => {
      return response.json();
    });
    return UsersLiterature;
  };

export default getUserLiterature
  