import React, { useState } from "react";
import { PrimaryButton, IButtonStyles } from "office-ui-fabric-react";
import Tts from "../components/tts";
import Speech from "../components/speech";
import { Books } from "../interfaces/books.interface";
import { Literature } from "../interfaces/literature.interface";
import getUserLiterature from "./getUserLiterature";
import readBook from "./readBook";
import { backend_server, backend_port } from "../settings";

const buttonStyle: IButtonStyles = {
  root: {
    width: 400,
    height: 400,
    marginTop: "10%",
    borderRadius: 1000,
    boxShadow:
      "0 1.6px 3.6px 0 rgba(0,0,0,0.132), 0 0.3px 0.9px 0 rgba(0,0,0,0.108)",
    selectors: {
      ":hover": {
        boxShadow:
          "0 6.4px 14.4px 0 rgba(0,0,0,0.132), 0 1.2px 3.6px 0 rgba(0,0,0,0.108)",
      },
    },
  },
};

const getBooks = () => {
  return fetch(`https://${backend_server}:${backend_port}/books`).then(
    (response) => {
      return response.json();
    }
  );
};

const startReadingNewBook = async (
  bookNumber: string,
  literature: Literature[],
  userID: string | undefined,
  setBookBeingRead: Function
) => {
  let continueBook: number = 0;
  if (bookNumber.toLowerCase().search(/jeden|jedna|1/) !== -1) {
    continueBook = 0;
  } else if (bookNumber.toLowerCase().search(/dva|2/) !== -1) {
    continueBook = 1;
  } else if (bookNumber.toLowerCase().search(/tri|3/) !== -1) {
    continueBook = 2;
  } else if (bookNumber.toLowerCase().search(/štyri|4/) !== -1) {
    continueBook = 3;
  } else if (bookNumber.toLowerCase().search(/päť|5/) !== -1) {
    continueBook = 4;
  }

  const newOffset: number = await readBook(userID, literature[continueBook]);
  literature[continueBook].offset = newOffset;
  setBookBeingRead(literature[continueBook]);
};

type Props = {
  user: string | undefined;
};
const MainButton = (props: Props) => {
  const [menuOption, setMenuOption] = useState<string>("init");
  const [literature, setLiterature] = useState<Literature[]>([]);
  const [isFirstRun, setIsFirstRun] = useState<boolean>(true);
  const [isBookBeingRead, setIsBookBeingRead] = useState<boolean>(false);
  const [bookBeingRead, setBookBeingRead] = useState<Literature>(
    {} as Literature
  );
  const getMenu = async (itranscript: string) => {
    let menuText = "";

    switch (menuOption) {
      case "init":
        setMenuOption("search-or-continue");
        menuText =
          "Pre vyhľadávanie publikácií, , stlačte tlačidlo znovu a povedzte Hľadaj. Pre pokračovanie v už začatej knihe, povedzte pokračuj";
        Tts(menuText); // prehraj odozvu
        break;
      case "search-or-continue":
        if (itranscript.toLowerCase().search("hľadaj") !== -1) {
          setMenuOption("search");
          menuText =
            "Pre vyhľadávanie podľa mena autora, povedzte autor. Pre vyhľadávanie podľa názvu publikácie, povedzte názov.";
          Tts(menuText);

          break;
        } else if (itranscript.toLowerCase().search("pokračuj") !== -1) {
          const literature: Literature[] = await getUserLiterature(props.user);
          if (literature.length) {
            const readingList: any = [];
            literature.forEach((book, index) => {
              readingList.push(
                `číslo ${index + 1} od autora ${book.author} s názvom ${
                  book.title
                }`
              );
            });
            menuText =
              "Povedzte číslo možnosti, v ktorej chcete pokračovať: " +
              readingList;
            console.log(menuText);
            Tts(menuText);
            setLiterature(literature);
            setMenuOption("continue");
            break;
          } else {
            menuText =
              "Nemáte rozčítané žiadne knihy, skúste radšej vyhľadávať.";
            Tts(menuText);
            setMenuOption("init");
            setIsFirstRun(true);
            break;
          }
        } else {
          menuText =
            "Nerozumel som. Pre vyhľadávanie publikácií, povedzte Hľadaj. Pre pokračovanie v už začatej knihe, povedzte pokračuj";
          Tts(menuText);
          break;
        }
      case "search":
        if (itranscript.toLowerCase().search("autor") !== -1) {
          setMenuOption("searchByAuthor");
          const uniqueAuthors = await getBooks().then((books: Books[]) => {
            const authors = books.map((book: Books) => book.author);
            
            return Array.from(new Set(authors));
          });
          menuText = `Vyhľadávam podľa autora. Na výber máte z týchto autorov. ${uniqueAuthors.map(author => { return `${author}. `})}. Prosím povedzte meno.`;
          console.log(menuText);
          Tts(menuText);
          break;
        } else if (itranscript.toLowerCase().search("názov") !== -1) {
          setMenuOption("searchByName");
          menuText =
            "Vyhľadávam podľa názvu publikácie. Prosím povedzte názov.";
          Tts(menuText);
          break;
        } else {
          menuText =
            "Nerozumiem. Pre vyhľadávanie podľa mena autora, povedzte autor. Pre vyhľadávanie podľa názvu publikácie, povedzte názov.";
          Tts(menuText);
          break;
        }
      case "searchByAuthor": // hladat podla autora
        const booksByAuthor = await getBooks().then((books: Books[]) => {
          return books.filter(function (entry: Books) {
            if (
              entry.author.toLowerCase().search(itranscript.toLowerCase()) !==
              -1
            )
              return true;
            // if part of authors name is in the transcript add it to the list
            else return false;
          });
        });
        if (booksByAuthor.length) {
          const temp: any = [];
          booksByAuthor.forEach((book, index) => {
            temp.push(
              `číslo ${index + 1} od autora ${book.author} s názvom ${
                book.title
              }`
            );
          });
          menuText = `Našiel som tieto knihy: ${temp} pre začatie čítania povedzte číslo knihy`;
          const literature: Literature[] = booksByAuthor.map((book) => {
            return {
              _id: book._id,
              offset: 0,
              author: book.author,
              title: book.title,
            };
          });
          setLiterature(literature);
          setMenuOption("continue");
        } else {
          menuText = "Žiadna taká kniha nieje, skúste zadať iného autora";
        }
        Tts(menuText);
        break;
      case "searchByName": // hladat podla nazvu
        const booksByTitle = await getBooks().then((books: Books[]) => {
          return books.filter(function (entry: Books) {
            if (
              entry.title.toLowerCase().search(itranscript.toLowerCase()) !== -1
            )
              return true;
            // if part of the title is in the transcript add it to the list
            else return false;
          });
        });

        if (booksByTitle.length) {
          const temp: any = [];
          booksByTitle.forEach((book, index) => {
            temp.push(
              `číslo ${index + 1} od autora ${book.author} s názvom ${
                book.title
              }`
            );
          });
          const literature: Literature[] = booksByTitle.map((book) => {
            return {
              _id: book._id,
              offset: 0,
              author: book.author,
              title: book.title,
            };
          });
          menuText = `Našiel som tieto knihy: ${temp} pre začatie čítania povedzte číslo knihy`;
          setLiterature(literature);
          setMenuOption("continue");
        } else {
          menuText = "Žiadna taká kniha nieje, skúste zadať iný názov";
        }
        Tts(menuText);
        break;
      case "continue": // pokracujem v jednej z knih
        if (isBookBeingRead === false) {
          startReadingNewBook(
            itranscript,
            literature,
            props.user,
            setBookBeingRead
          );
          setIsBookBeingRead(true);
        } else {
          const newOffset: number = await readBook(props.user, bookBeingRead);
          bookBeingRead.offset = newOffset;
          setBookBeingRead(bookBeingRead);
        }
        break;
      case "startReading":
    }

    console.log("After Menu text is " + menuText);
  };
  const HandleSpeech = () => {
    if (isFirstRun === true) {
      setIsFirstRun(false);
      getMenu("");
    } else if (isBookBeingRead === true) {
      getMenu("");
    } else Speech(getMenu);
  };

  return (
    <div>
      <PrimaryButton onClick={HandleSpeech} styles={buttonStyle}>
        Stlač ma!
      </PrimaryButton>
    </div>
  );
};

export default MainButton;
