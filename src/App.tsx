import React, { useEffect} from "react";
import Library from "./components/Library";
import Home from "./components/Home";
import NavBar from "./components/NavBar";
import { Stack } from "@fluentui/react";
import { Route, Redirect, Switch } from "react-router-dom";
import UserContext from "./lib/user";
import Login from "./components/login";

const App = () => {
  const [user, setUser] = React.useState(
    localStorage.getItem("user") || undefined
  );
  useEffect(() => {
    document.title = "Knižnica"
 }, []);
  const PrivateRoute = ({ component: Component, path: Path, ...options }: any) => {
    //it checks if user is created, if yes it loads the correct component
    //otherwise loads the login page
    return (
      <>
        {user != null ? (
          <Route {...options} path={Path} component={Component} />
        ) : (
          <Route render={(props) => <Login {...props} setUser={setUser} />} />
        )}
      </>
    );
  };

  return (
    <UserContext.Provider value={[user]}>
    
        {user != null && <NavBar setUser={setUser}></NavBar>}
        <Stack>
          <Switch>
            {/* user has to be authenticated to access these components */}
            {/* <Route path="/home" render={} /> */}
            <PrivateRoute path="/library" component={Library} />
            <PrivateRoute path="/home" component={() => <Home user={user}/>} />
            <Route path="/" render={() => <Redirect to={"/home"} />} />
          </Switch>
        </Stack>
    </UserContext.Provider>
  );
};

export default App;
