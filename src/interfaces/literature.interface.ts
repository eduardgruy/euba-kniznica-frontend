export interface Literature {
    _id: string;
    offset: number;
    author: string;
    title: string;
  }
  