export interface Books {
  _id: string;
  author: string;
  title: string;
  category: string;
  year: number;
  description: string;
  type: string;
}
